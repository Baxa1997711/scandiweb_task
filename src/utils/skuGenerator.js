export function generateSKU() {
    let sku = "";
    const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    
    // Add brand code to SKU
    sku += characters.charAt(Math.floor(Math.random() * 26));
    sku += characters.charAt(Math.floor(Math.random() * 26));
    sku += characters.charAt(Math.floor(Math.random() * 26));
    
    // Add product code to SKU
    sku += characters.charAt(Math.floor(Math.random() * 26));
    sku += characters.charAt(Math.floor(Math.random() * 26));
    sku += characters.charAt(Math.floor(Math.random() * 26));
    
    // Add unique identifier to SKU
    sku += Math.floor(Math.random() * 10000);
    
    return sku;
  }