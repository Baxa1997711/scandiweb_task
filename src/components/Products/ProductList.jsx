import React from 'react';
import styles from './styles.module.scss'
 
function ProductList({ productList = [], handleCheck, selectedProducts = [] }) {
    return (
        <div className={styles.productList}>
            {productList?.length ? 
            (productList?.map((item, index) => (
                <div key={index} className={styles.productListItem}>
                <div className={styles.productChecked}>
                    <input type="checkbox" className='checkbox' checked={selectedProducts?.some((el) => item.id === el.id && item?.type === el?.type)} 
                    onChange={() => {
                        handleCheck(item)
                    }}/>
                </div>
                <div className={styles.productInfo}>
                        <h2 className={styles.product_sku}>{item?.sku}</h2>
                        <h2>{item?.name}</h2>
                        <h2>{`${item?.price}.00 $`}</h2>
                        <h2>{item?.type === 'dvd' && `Size: ${item?.size}MB`}</h2>
                        <h2>{item?.type === 'book' && `Weight: ${item?.weight}KG`}</h2>
                        <h2>{item?.type === 'furniture' && `Dimension: ${item?.width}x${item?.height}x${item?.length}`}</h2>
                </div>
            </div>
            ))) : (
                <div className={styles.no_data}>NO DATA...</div>
            )}
        </div>
    );
}

export default ProductList;