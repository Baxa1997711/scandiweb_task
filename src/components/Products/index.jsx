import React, { useEffect, useState } from "react";
import ProductList from "./ProductList";
import styles from './styles.module.scss'
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function Products() {
  const navigate = useNavigate()
  const [productList, setProductList] = useState([])
  const [selectedProducts, setSelectedProducts] = useState([])
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  const createPage = () => {
    navigate('/addproduct')
  }
  const notifyError = () => toast.error(createError);
  const notifyDelete = () => toast.info('Products Successfully deleted');

  const getProducts = async () => {
    try {
      const res = await axios.get('https://admin-baha.uz')
      setProductList(res?.data);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      setError(err);
      notifyError()
    }
  }

  const handleCheck = (product) => {
    if (
      selectedProducts?.some(
        (item) => item.id === product.id && item.type === product.type
      )
    ) {
      setSelectedProducts(
        selectedProducts.filter(
          (item) => item.id !== product.id || item.type !== product.type
        )
      );
    } else {
      setSelectedProducts([...selectedProducts, product]);
    }
  };
  
  const massDelete = async () => {
    try {
      await axios.delete('https://admin-baha.uz/', { data: selectedProducts });
      getProducts();
      setSelectedProducts([]);
      notifyDelete();
    } catch (err) {
      setError(err);
    }
  }

  useEffect(() => {
    getProducts()
  }, [])

  if (loading) {
    return <p>Loading...</p>;
  }

  if (error) {
    return <p>Something went wrong: {error.message}</p>;
  }

  return (
    <div className={styles.products}>
      <div className={styles.producthead}>
        <h2>Product List</h2>

        <div className={styles.controlButtons}>
          <button className={styles.addButton} onClick={createPage}>Add</button>
          <button className={styles.deleteButton} onClick={massDelete} id='delete-product-btn'>Mass Delete</button>
        </div>
      </div>
      <ProductList productList={productList} selectedProducts={selectedProducts} handleCheck={handleCheck}/>
      <ToastContainer autoClose={4000} position="top-right" />
    </div>
  )
}

export default Products;
