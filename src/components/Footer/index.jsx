import React from 'react';
import styles from './styles.module.scss'

function Footer(props) {
    return (
        <div className={styles.footer}>
            <h2>Scandiweb Test assignment.</h2>
        </div>
    );
}

export default Footer;