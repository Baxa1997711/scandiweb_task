import React from 'react';
import Footer from '../Footer';
import Products from '../Products';
import styles from './styles.module.scss'

function Main(props) {
    return (
        <div className={styles.main}>
          <Products/> 
          <Footer/>
        </div>
    );
}

export default Main;