import React, { useEffect, useState } from "react";
import styles from "./styles.module.scss";
import { useNavigate } from "react-router-dom";
import Footer from "../Footer";
import Select from "react-select";
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


function ProductCreate(props) {
  const navigate = useNavigate();
  const [type, setType] = useState('');
  const [values, setValues] = useState({});
  const [createError, setCreateError] = useState('')

  const cancelCreate = () => {
    navigate(-1);
  };

  const defineType = (item) => {
    setType(item?.value);
  };
  
  const notifyError = () => toast.error(createError ? createError : 'Product with the same SKU already exists');
  
  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setValues((values) => ({ ...values, type: type, [name]: value }));
  };
  
  
  const handleSubmit = (e) => {
    e.preventDefault();
    const val = e.target.value;

    if(values?.type && values?.name && values?.price) {
      axios.post('https://admin-baha.uz/', JSON.stringify(values))
    .then((res) => {
      console.log('ressssss', res)
      if(res?.data?.status === 0){
        setCreateError(res?.data?.message)
        notifyError()
      } else {
        cancelCreate()
      }
    }).catch((err) => {
    });
    } else {
      alert("Please, submit required data");
    }
  };

  const dataMock = [
    {
      label: "DVD",
      value: "dvd",
    },
    {
      label: "Furniture",
      value: "furniture",
    },
    {
      label: "Book",
      value: "book",
    },
  ];

  return (
    <>
      <div className={styles.addproduct}>
        <div className={styles.producthead}>
          <h2>Product List</h2>

          <div className={styles.controlButtons}>
            <button className={styles.addButton} onClick={handleSubmit}>
              Save
            </button>
            <button
              className={styles.deleteButton}
              id="delete-product-btn"
              onClick={cancelCreate}
            >
              Cancel
            </button>
          </div>
        </div>
        <form className={styles.product_form} id="product_form" method="POST">
          <div>
            <div className={styles.product_form_input}>
              <label for="sku">SKU</label>
              <input
                id="sku"
                type="text"
                name="sku"
                placeholder="sku"
                onChange={handleChange}
              />
            </div>
            <div className={styles.product_form_input}>
              <label for="name">Name</label>
              <input
                id="name"
                type="text"
                name="name"
                placeholder="name"
                onChange={handleChange}
                required
              />
            </div>
            <div className={styles.product_form_input}>
              <label for="price">Price ($)</label>
              <input
                id="price"
                name="price"
                placeholder="price ($)"
                onChange={handleChange}
                required
              />
            </div>
          </div>

          <div className={styles.productType}>
            <h2>Type Swticher</h2>
            <Select
              styles={{ width: "200px" }}
              name="type"
              options={dataMock}
              onChange={defineType}
              placeholder={"Type Swticher..."}
              required
            />
          </div>

          <div className={styles.productAttributes}>
            {type === "dvd" ? (
              <div className={styles.attributes}>
                <h2>DVD</h2>
                <input
                  type="text"
                  placeholder="size (MB)"
                  name="size"
                  id="DVD"
                  onChange={handleChange}
                />
              </div>
            ) : type === "furniture" ? (
              <div className={styles.attributes}>
                <h2>Furniture</h2>
                <input
                  type="text"
                  placeholder="height (CM)"
                  name="height"
                  id="Furniture"
                  onChange={handleChange}
                />
                <input
                  type="text"
                  placeholder="width (CM)"
                  name="width"
                  id="Furniture"
                  onChange={handleChange}
                />
                <input
                  type="text"
                  placeholder="length (CM)"
                  name="length"
                  id="Furniture"
                  onChange={handleChange}
                />
              </div>
            ) : type === "book" ? (
              <div className={styles.attributes}>
                <h2>Book</h2>
                <input
                  type="text"
                  placeholder="weight (KG)"
                  name="weight"
                  id="Book"
                  onChange={handleChange}
                />
              </div>
            ) : (
              ""
            )}
          </div>
        </form>
        <ToastContainer autoClose={4000} position="top-right" />
      </div>
      <Footer />
    </>
  );
}

export default ProductCreate;
