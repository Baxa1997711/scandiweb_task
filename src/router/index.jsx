import React, { Suspense } from "react";
import { Routes, Route, Navigate, useLocation } from "react-router-dom";
import Main from "../components/Main";
import ProductCreate from "../components/ProductCreate";

const Router = () => {
  return (
    <Routes>
      <Route path="/" element={<Main />} />
      <Route path='/addproduct' element={<ProductCreate/>}/>
    </Routes>
  );
};
export default Router;
